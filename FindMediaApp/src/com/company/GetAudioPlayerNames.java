package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GetAudioPlayerNames extends SuggestionsTables {
    ResultSet rs;
    protected String audioPlayerNames[] = new String[20];
    private String temp;
    private String query1;

    public GetAudioPlayerNames(){
        this.query1="SELECT PlayerName FROM MediaPlayer WHERE FileType='mp3'";
    }
    public void valuesFromDB()throws NullPointerException{
        try {
            CreateConnection conn = new CreateConnection();
            Connection c = conn.getConnection();
            Statement st = c.createStatement();
            rs = st.executeQuery(query1);
            {
                int i = 0;
                while (rs.next()) {
                    temp = rs.getString(1);
                    audioPlayerNames[i] = temp;
                    i++;
                }
            }
            c.close();
        }catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void display(){
        for(int i=0;audioPlayerNames[i]!=null;i++){
            System.out.println(" "+audioPlayerNames[i]);
        }
    }
}
