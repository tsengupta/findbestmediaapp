package com.company;


public class GetTypeOfFile {

    String extention;

    public  GetTypeOfFile(String extention)
    {
        this.extention=extention;
    }

    public String compareAudioVideo(){
        String[] extentionsAudio={"mp3"};
        String[] extentionsVideo={"mp4"};

        try{
            for(int i=0;i<extentionsAudio.length;i++)
            {
                if(extentionsAudio[i].equals(extention)) {
                    return ("Audio");
                }
                }

                for(int i=0;i<extentionsVideo.length;i++)
                {
                if(extentionsVideo[i].equals(extention))
                {
                    return("Video");
                }
                else{
                    System.out.println("not audio or video");
                }
                }
        }catch(NullPointerException e){
            System.out.println(e);
        }catch (IllegalArgumentException b){
            System.out.println(b);
        }
        return(null);
    }
}
