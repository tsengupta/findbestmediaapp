package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GetVideoPlayerNames extends SuggestionsTables {
    ResultSet rs;
    protected String videoPlayerNames[] = new String[20];
    private String temp;
    private String query1;

    public GetVideoPlayerNames(){
        this.query1="SELECT PlayerName FROM MediaPlayer WHERE FileType='mp4'";
    }

    public void valuesFromDB(){
        try {
            CreateConnection conn = new CreateConnection();
            Connection c = conn.getConnection();
            Statement st = c.createStatement();
            rs = st.executeQuery(query1);
            {
                int i = 0;
                while (rs.next()) {
                    temp = rs.getString(1);
                    videoPlayerNames[i] = temp;
                    i++;
                }
            }
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public void display(){
        for(int i=0;videoPlayerNames[i]!=null;i++){
            System.out.println(" "+videoPlayerNames[i]);
        }
    }
}
